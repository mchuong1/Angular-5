import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers', //Selectby Element
  //selector: '[app-servers]', Select by attribute
  //selector: '.app-servers', Select by Class
 // template: `
 //<app-server></app-server>
 //<app-server></app-server>`,
  templateUrl: './servers.component.html',    
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No Server was created';
  serverName = 'TestServer';
  serverCreated = false;
  servers =['Testserver', 'Testserver 2'];
  
  constructor() { 
   setTimeout(() =>{
     this.allowNewServer = true;
   } ,2000);
  }

  ngOnInit() {
  }

  onCreateServer(){
    this.serverCreated = true;
    this.servers.push(this.serverName);
    this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
    }
 // onUpdateServerName(event: Event){
//    this.serverName = <HTMLInputElement>event.target.value;
//  }
}
